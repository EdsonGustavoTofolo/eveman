package br.com.edsontofolo.eveman.EveMan;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EveManApplication {

	public static void main(String[] args) {
		SpringApplication.run(EveManApplication.class, args);
	}
}
